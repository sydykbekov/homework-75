import React, {Component, Fragment} from 'react';
import './App.css';
import {connect} from "react-redux";
import {changeMessage, decode, encode} from "./store/action";
import Preloader from './assets/Preloader.gif';

class App extends Component {
    render() {
        return (
            <Fragment>
                <img src={Preloader} alt="loading" style={{display: this.props.loading ? 'block' : 'none'}}/>
                <div className="App">
                    <div className="box">
                        <h4>Decoded message</h4>
                        <textarea name="decoded" value={this.props.decodedMessage}
                                  onChange={this.props.changeMessage} cols="30" rows="10"/>
                        <button onClick={this.props.encode}>Encode ></button>
                    </div>
                    <div className="box">
                        <h4>Password</h4>
                        <input value={this.props.password} onChange={this.props.changeMessage} name="password"
                               type="text"/><br/>
                    </div>
                    <div className="box">
                        <h4>Encoded message</h4>
                        <textarea name="encoded" value={this.props.encodedMessage}
                                  onChange={this.props.changeMessage} cols="30" rows="10"/>
                        <button onClick={this.props.decode}>{"<"} Decode</button>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        encodedMessage: state.encoded,
        decodedMessage: state.decoded,
        password: state.password,
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        encode: () => dispatch(encode()),
        decode: () => dispatch(decode()),
        changeMessage: (event) => dispatch(changeMessage(event))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
