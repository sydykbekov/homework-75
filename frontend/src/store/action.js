import axios from 'axios';

export const START_REQUEST = 'START_REQUEST';
export const SUCCESS_REQUEST = 'SUCCESS_REQUEST';
export const ERROR_REQUEST = 'ERROR_REQUEST';
export const CHANGE_MESSAGE = 'CHANGE_MESSAGE';

export const changeMessage = event => {
    event.persist();
    return {type: CHANGE_MESSAGE, event};
};

const startRequest = () => {
    return {type: START_REQUEST};
};

const successRequest = (response) => {
    return {type: SUCCESS_REQUEST, value: response};
};

const errorRequest = () => {
    return {type: ERROR_REQUEST};
};

export const encode = () => {
    return (dispatch, getState) => {
        const object = {message: getState().decoded, password: getState().password};
        if (object.password === '') {
            return alert('Please fill in the password field!');
        }
        dispatch(startRequest());
        axios.post('encode', object).then(response => {
            dispatch(successRequest(response.data));
        }, error => {
            dispatch(errorRequest());
        });
    };
};

export const decode = () => {
    return (dispatch, getState) => {
        const object = {message: getState().encoded, password: getState().password};
        if (object.password === '') {
            return alert('Please fill in the password field!');
        }
        dispatch(startRequest());
        axios.post('decode', object).then(response => {
            dispatch(successRequest(response.data));
        }, error => {
            dispatch(errorRequest());
        });
    };
};