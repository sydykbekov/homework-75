import {CHANGE_MESSAGE, ERROR_REQUEST, START_REQUEST, SUCCESS_REQUEST} from './action';

const initialState = {
    encoded: '',
    decoded: '',
    password: '',
    loading: false
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_MESSAGE:
            return {...state, [action.event.target.name]: action.event.target.value};
        case START_REQUEST:
            return {...state, loading: true};
        case SUCCESS_REQUEST:
            const method = Object.keys(action.value);
            return {...state, loading: false, [Object.keys(action.value)]: action.value[method]};
        case ERROR_REQUEST:
            return {...state, loading: false};
        default:
            return state;
    }
};

export default reducer;