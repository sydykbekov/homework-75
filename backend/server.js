const express = require('express');
const app = express();
const Vigenere = require('caesar-salad').Vigenere;
const cors = require('cors');
const port = 8000;

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send('Hello, world!');
});

app.post('/encode', (req, res) => {
    let word = Vigenere.Cipher(req.body.password).crypt(req.body.message);
    const reply = {encoded: word};
    res.send(reply);
});

app.post('/decode', (req, res) => {
    let word = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    const reply = {decoded: word};
    res.send(reply);
});

app.listen(port, () => {
    console.log('We are live on ' + port);
});